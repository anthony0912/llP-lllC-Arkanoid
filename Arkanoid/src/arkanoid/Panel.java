/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Anthony
 */
public class Panel extends JPanel implements KeyListener {
    
    private Logica logica;

    public Panel() {
        setPreferredSize(new Dimension(600, 700));
        addKeyListener(this);
        setFocusable(true);
        logica = new Logica();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        logica.paint(g);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_UP:
                logica.getRaqueta().setMover(0);
                break;
            case KeyEvent.VK_DOWN:
                logica.getRaqueta().setMover(0);
                break;
            case KeyEvent.VK_LEFT:
                logica.getRaqueta().setMover(3);
                break;
            case KeyEvent.VK_RIGHT:
                logica.getRaqueta().setMover(4);
                break;
            case KeyEvent.VK_P:
                logica.setEsPausa(false);
                logica.getBola().setPausa(logica.getBola().getPausa() + 1);
                if (logica.getBola().getPausa() == 2) {
                    logica.getBola().setPausa(0);
                    logica.setEsPausa(true);
                }
                break;
            case KeyEvent.VK_SPACE:
                logica.setEsInicio(true);
                logica.setPerdioTurno(true);
                logica.setSiguientNivel(true);
                logica.setTocoCapsulaRaqueta(false);
                if (logica.isTocoCapsula() && logica.getContTurnosLaser() == 0) {
                    logica.setActivarLaser(true);
                    logica.setContTurnosLaser(logica.getContTurnosLaser()+1);
                }
                break;
            case KeyEvent.VK_R:
                logica.setReiniciar(true);
                break;
        }
        logica.getBola().mover(ke.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                logica.getRaqueta().setMover(0);
                break;
            case KeyEvent.VK_RIGHT:
                logica.getRaqueta().setMover(0);
                break;
        }
    }

}
