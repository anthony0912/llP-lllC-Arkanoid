/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author Anthony
 */
public class Logica {

    private final int ANCHO_RAQ = 30;
    private final int ALTO_RAQ = 10;
    private final int RAQY = 660;
    private final int BOLAY = 650;
    private final int SPEED = 2;
    private Rectangulo borde;
    private Rectangulo fondo;
    private Rectangulo tabla;
    private Raqueta raqueta;
    private Mensaje pintarTabla;
    private Circulo bola;
    private Color cb;
    private int tiempo;
    private boolean esInicio;
    private boolean esPausa;
    private boolean perdioTurno;
    private boolean reiniciar;
    private char[] tipoBloque;
    private char[] tipoCapsula;
    private Mapa[][] mapa;
    private Rectangulo[] vidas;
    private Capsula[] capsula;
    private int vida;
    private int puntaje;
    private int nivel;
    private int nivelPantalla;
    private int espacLlenos;
    private int cont;
    private boolean siguientNivel;
    private boolean intersectoConRaqueta;
    private boolean tocoCapsulaRaqueta;
    private boolean pasoTiempo;
    private int contTimer;
    private int speedPuntos;
    private boolean activarLaser;
    private LinkedList<Laser> laser1;
    private LinkedList<Laser> laser2;
    private int contTurnosLaser;
    private boolean tocoCapsula;

    public Logica() {
        cb = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
        borde = new Rectangulo(0, 0, 600, 700, cb, true);
        tabla = new Rectangulo(5, 5, 590, 90, Color.BLACK, true);
        pintarTabla = new Mensaje(18, Color.WHITE);
        fondo = new Rectangulo(5, 100, 590, 595, Color.BLACK, true);
        raqueta = new Raqueta(0, RAQY, ANCHO_RAQ, ALTO_RAQ, Color.GRAY, Color.WHITE, 0);
        bola = new Circulo(0, BOLAY, 5, Color.WHITE, true, 1, 1, SPEED, true, true);
        tipoBloque = new char[]{'b', 'y', 'w', 'o', 'c', 'g', 'r', 'a', 'm'};
        tipoCapsula = new char[]{'a', 'r', 'g', 'v', 'n'};
        mapa = new Mapa[9][14];
        vidas = new Rectangulo[4];
        capsula = new Capsula[3];
        puntaje = 0;
        nivel = 1;
        nivelPantalla = 1;
        vida = 4;
        espacLlenos = 0;
        cont = 0;
        contTimer = 0;
        speedPuntos = 0;
        config();
        laser1 = new LinkedList<Laser>();
        laser2 = new LinkedList<Laser>();
        setContTurnosLaser(0);
        
    }

    public void paint(Graphics g) {
        borde.paint(g);
        tabla.paint(g);
        pintarTabla.paint(g);
        fondo.paint(g);
        pintarBlock(g);
        colicionBlockes();
        if (esTocarBlocke()) {
            pintarCapsulas(g);
        }
        raqueta.paint(g);
        bola.paint(g);
        mostrarVidas(g);
        colisionInferiorPantalla();
        bolaLenta();
        if (isPasoTiempo()) {
            aumentarSpeed();
        }
        getBoundsCapsula();
        if (isEsInicio() && isPerdioTurno() && isSiguientNivel()) {
            if (!isEsPausa()) {
                g.drawString(pintarTabla.pausa, 280, 325);
            }
            if (isEsPausa()) {
                iniciarJuego();
            }
        } else if (!isEsInicio()) {
            configInicial();
            g.drawString(pintarTabla.mensajeInico, 115, 325);
        } else if (!isPerdioTurno()) {
            configInicial();
            g.drawString(pintarTabla.comPartida, 145, 325);
        } else if (!isSiguientNivel()) {
            resetiarLaser();
            configInicial();
            g.drawString(pintarTabla.siguentNivel, 115, 325);
        }
        if (isReiniciar()) {
            reiniciarJuego();
            setReiniciar(false);
        }
        g.setFont(new Font("ArcadeClassic", Font.BOLD, 30));
        g.drawString(Integer.toString(puntaje), 20, 60);
        g.drawString(Integer.toString(nivelPantalla), 160, 60);
        moverBola();
        canonLaser(g);
        intersectoLaser1();
        intersectoLaser2();
        tiempoLaser();
    }

    /**
     * metodo que realiza la configucion de los mapas
     */
    private void config() {
        posInicialBolaYRaqueta();
        formatiarMatiz();
        vidas();
        crearFigura();
        configPosicioBlockes();
        randomCapsulas();
        posBlockSinNull();
        setPasoTiempo(true);
        contTimer = 0;
    }

    /**
     * metodo que reinicia el juego
     */
    private void reiniciarJuego() {
        puntaje = 0;
        espacLlenos = 0;
        vida = 4;
        nivel = 1;
        nivelPantalla = 1;
        cont = 0;
        config();
        setEsInicio(false);
        setPerdioTurno(true);
        setSiguientNivel(true);
        raqueta.setBase(ANCHO_RAQ);
    }

    /**
     * metodo con la confinguracion inicial de la bola y la barra
     */
    private void configInicial() {
        bola.mover(0);
        bola.setX(raqueta.getX() + 40);
        bola.setSpace(0);
        raqueta.mover();
        setEsPausa(true);
    }

    /**
     * metodo donde estan todos los movimientos del panel
     */
    private void iniciarJuego() {
        bola.mover(600, 700);
        raqueta.mover();
        randomColorBorde();
    }

    /**
     * metodo que me configura los patrones del mapa
     *
     * @param g Graphics
     */
    private void configPosicioBlockes() {
        int cant = 0;
        int xs = 14;
        int ys = 150;
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                if (mapa[x][y] != null && (mapa[x][y].getTipoPatron() == 1 && (y % 3 != 0 && x % 2 == 0)
                        || mapa[x][y].getTipoPatron() == 2 && ((y % 2 != 0 && x % 2 == 0) || (y % 2 == 0 && x % 2 != 0))
                        || mapa[x][y].getTipoPatron() == 3 && ((y % 4 == 0 && x % 4 == 0) || (y % 2 != 0 && x % 2 != 0))
                        || mapa[x][y].getTipoPatron() == 4 && ((y % 3 == 0 && x % 2 == 0) || (y % 2 != 0 && x % 4 != 0))
                        || mapa[x][y].getTipoPatron() == 5)) {
                    if (mapa[x][y].isEsColicion()) {
                        mapa[x][y].setX(xs);
                        mapa[x][y].setY(ys);
                        if (cont == 0) {
                            espacLlenos++;
                        }
                    }
                } else {
                    mapa[x][y] = null;
                }
                xs += 41;
            }
            xs = 14;
            ys += 16;
        }
        cont++;
    }

    /**
     * metodo que llena la información de los blockes
     */
    private void crearFigura() {

        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                if (mapa[x][y] == null) {
                    char tipoBloq = tipoBloque[(int) (Math.random() * (tipoBloque.length))];
                    int cantColiciones = 0;
                    int puntos = 0;
                    switch (tipoBloq) {
                        case 'b':
                            cantColiciones = 3;
                            puntos = 500;
                            break;
                        case 'a':
                            cantColiciones = 2;
                            puntos = 300;
                            break;
                        default:
                            cantColiciones = 1;
                            puntos = 100;
                            break;
                    }
                    mapa[x][y] = new Mapa(0, 0, cantColiciones,
                            nivel, tipoBloq, 'n', true, puntos);
                }
            }
        }

    }

    /**
     * metodo boolean cuando toca el blocke donde la capsula se muestre
     *
     * @return true: toco el blocke donde estaba la capsula, false: no esta la
     * capsula
     */
    private boolean esTocarBlocke() {
        for (int i = 0; i < capsula.length; i++) {
            if (capsula[i] != null && capsula[i].isTocoBlocke()) {
                return true;
            }
        }
        return false;
    }

    /**
     * metodo que pinta los blockes
     *
     * @param g Graphics
     */
    private void pintarBlock(Graphics g) {
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                if (mapa[x][y] != null) {
                    mapa[x][y].paint(g);
                }
            }
        }
    }

    /**
     * metodo que realiza el cambio de los colores del borde de la ventana
     */
    private void randomColorBorde() {
        tiempo += 10;
        if (tiempo % 3000 == 0) {
            cb = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
            borde.setColor(cb);
        }
    }

    /**
     * metodo que se encarga de mover la bola cuando intersecta con la con la
     * barra
     */
    private void moverBola() {
        if (bola.getBounds().intersects(raqueta.getBounds(1))) {
            if (!isTocoCapsulaRaqueta()) {
                bola.mover(1);
                bola.mover(3);
            } else if (isTocoCapsulaRaqueta()) {
                detenerBola();
            }
        } else if (bola.getBounds().intersects(raqueta.getBounds(2))) {
            if (!isTocoCapsulaRaqueta()) {
                bola.mover(1);
            } else if (isTocoCapsulaRaqueta()) {
                detenerBola();
            }
        } else if (bola.getBounds().intersects(raqueta.getBounds(3))) {
            if (!isTocoCapsulaRaqueta()) {
                bola.mover(1);
                bola.mover(4);
            } else if (isTocoCapsulaRaqueta()) {
                detenerBola();
            }
        }
    }

    /**
     * metodo que me formatea las posiciones donde se almaceno los datos en la
     * matriz
     */
    private void formatiarMatiz() {
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                mapa[x][y] = null;
            }
        }
    }

    /**
     * metodo cuando la bola intersecta con las figuras del mapa
     */
    private void colicionBlockes() {
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                if (mapa[x][y] != null && bola.getBounds().intersects(mapa[x][y].getBounds())) {
                    mapa[x][y].setCantColiciones(mapa[x][y].getCantColiciones() - 1);
                    if (mapa[x][y].getCantColiciones() == 0) {
                        mapa[x][y].setEsColicion(false);
                        if (!mapa[x][y].isEsColicion()) {
                            if (estaCapsula(x, y) != (-1)) {
                                capsula[estaCapsula(x, y)].setTocoBlocke(true);
                            }
                            puntaje += mapa[x][y].getPuntos();
                            mapa[x][y] = null;
                            espacLlenos--;
                            if (espacLlenos == 0) {
                                siguienteNivel();
                            }
                        }
                    }
                    if (bola.getSpeedY() > 0) {
                        bola.mover(1);
                    } else if (bola.getSpeedY() < 0) {
                        bola.mover(2);
                    } else if (bola.getSpeedX() > 0) {
                        bola.mover(4);
                    } else if (bola.getSpeedX() < 0) {
                        bola.mover(3);
                    }
                }
            }
        }
    }

    /**
     * metodo que de vuelve la posicion de la capsula
     *
     * @param x int columnas de la matriz
     * @param y int filas de la matriz
     * @return i: posicion de la capsula, -1: si se encuentra ninguna
     * coincidencia
     */
    private int estaCapsula(int x, int y) {
        for (int i = 0; i < capsula.length; i++) {
            if (capsula[i] != null && mapa[x][y] != null
                    && capsula[i].getX() == mapa[x][y].getX()
                    && capsula[i].getY() == mapa[x][y].getY()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * metodo que me genera los datos de la vida en el mapa
     */
    private void vidas() {
        for (int i = 1; i < vidas.length; i++) {
            if (vidas[i] == null || vidas[i].getColor() == null) {
                vidas[i] = new Rectangulo(0, 0, 40, 10, Color.RED, true);
            }
        }
    }

    /**
     * metodo que pinta las vidas
     *
     * @param g Graphics
     */
    private void mostrarVidas(Graphics g) {
        int x = 280;
        int y = 40;
        for (int i = 1; i < vidas.length; i++) {
            if (vidas[i] != null) {
                vidas[i].setX(x);
                vidas[i].setY(y);
                vidas[i].paint(g);
                g.setColor(Color.WHITE);
                g.drawRect(vidas[i].getX(), vidas[i].getY(), vidas[i].getBase(), vidas[i].getAltura());
            }
            x += 42;
        }
    }

    /**
     * metodo que verifica si la bola coliciona con la parte inferior de la
     * ventana
     */
    private void colisionInferiorPantalla() {
        if (vidas[vida - 1] != null && bola.getY() >= 695 - bola.getRadio()) {
            vidas[vida - 1].setColor(null);
            vida -= 1;
            contTimer = 0;
            posInicialBolaYRaqueta();
            setPasoTiempo(true);
            setEsInicio(true);
            setPerdioTurno(false);
            setSiguientNivel(true);
            raqueta.setBase(ANCHO_RAQ);
        } else if (vida == 1) {
            puntaje = 0;
            espacLlenos = 0;
            cont = 0;
            vida = 4;
            nivel = 1;
            nivelPantalla = 1;
            config();
            raqueta.setBase(ANCHO_RAQ);
        }
    }

    /**
     * metodo que indica la posicion de la bola y la raqueta despues haber
     * perdido
     */
    private void posInicialBolaYRaqueta() {
        bola.setX(raqueta.getBase() * 3 / 2);
        bola.setY(BOLAY);
        raqueta.setX(600 / 2 - raqueta.getBase() * 3 / 2);
        raqueta.setY(RAQY);
    }

    /**
     * metodo que carga el siguente mapa y cambia de nivel
     */
    private void siguienteNivel() {
        if (isSiguientNivel()) {
            if (nivel == 5) {
                nivel = 0;
            }
            vida = 4;
            espacLlenos = 0;
            nivel += 1;
            nivelPantalla += 1;
            cont = 0;
            raqueta.setBase(ANCHO_RAQ);
            config();
            setEsInicio(true);
            setPerdioTurno(true);
            setSiguientNivel(false);
        }
    }

    /**
     * metodo que cambia el speed de la barra y la bola
     *
     * @param speed int numero de velocidad
     */
    private void speedBolaYBarra(int speed) {
        bola.setSpeed(speed);
        raqueta.setSpeed(speed);
    }

    /**
     * metodo que aumenta la speed del la barra y la bola
     */
    private void aumentarSpeed() {
        if (puntaje == 0) {
            speedBolaYBarra(SPEED);
        } else if (puntaje > 8000 && puntaje < 12000) {
            speedBolaYBarra(3);
        } else if (puntaje > 12000 && puntaje < 20000) {
            speedBolaYBarra(4);
        } else if (puntaje > 30000 && puntaje < 40000) {
            speedBolaYBarra(5);
        } else if (puntaje > 40000) {
            speedBolaYBarra(6);
        }
        speedPuntos = bola.getSpeed();
    }

    /**
     * metodo que verifica los bloque estan llenos para rifar las capsulas
     */
    private void posBlockSinNull() {
        for (int i = 0; i < capsula.length; i++) {
            int posX = (int) (Math.random() * mapa.length) + 0;
            int posY = (int) (Math.random() * mapa[posX].length) + 0;
            if (mapa[posX][posY] == null || capsula[i] == null) {
                i--;
            } else {
                capsula[i].setX(mapa[posX][posY].getX());
                capsula[i].setY(mapa[posX][posY].getY());
            }
        }
    }

    /**
     * metodo que hace el random de las posiciones donde estaran ubicadas las
     * capsulas
     */
    private void randomCapsulas() {
        for (int i = 0; i < capsula.length; i++) {
            capsula[i] = new Capsula(0, 0, tipoCapsula[(int) (Math.random() * tipoCapsula.length) + 0], 2, false);
            for (int k = 0; k < i; k++) {
                if (capsula[k] != null
                        && capsula[k].getTipo() == capsula[i].getTipo()) {
                    i--;
                }
            }
        }
    }

    /**
     * metodo que me pinta las capsulas
     *
     * @param g Graphics
     */
    private void pintarCapsulas(Graphics g) {
        for (int i = 0; i < capsula.length; i++) {
            if (capsula[i] != null && capsula[i].isTocoBlocke()) {
                capsula[i].pintarCapsula(g);
                capsula[i].setMover(1);
                capsula[i].mover();
            }
        }
    }

    /**
     * metodo que se activa cuando la capsula intersecta con la raqueta
     */
    private void getBoundsCapsula() {
        for (int i = 0; i < capsula.length; i++) {
            if (capsula[i] != null
                    && (raqueta.getBounds(1).intersects(capsula[i].getBounds())
                    || raqueta.getBounds(2).intersects(capsula[i].getBounds())
                    || raqueta.getBounds(3).intersects(capsula[i].getBounds()))) {
                switch (capsula[i].getTipo()) {
                    case 'a':
                        expandorBarra();
                        break;
                    case 'r':
                        setTocoCapsula(true);
                        break;
                    case 'g':
                        aumentarVida();
                        break;
                    case 'v':
                        setTocoCapsulaRaqueta(true);
                        break;
                    case 'n':
                        setPasoTiempo(false);
                        break;
                    default:
                        break;
                }
                capsula[i] = null;
            }
        }
    }

    /**
     * metodo que expande la raqueta cuando intersecta una capsula azul
     */
    private void expandorBarra() {
        raqueta.setBase(ANCHO_RAQ * 2);
    }

    /**
     * metodo que aumenta una vida cuando intersecta una capsula gris
     */
    private void aumentarVida() {
        if (vida < 4) {
            vidas[vida].setColor(Color.RED);
            vida += 1;
        }
    }

    /**
     * metodo que hace mas lenta la bola cuando intersecta con una capsula
     * naranja
     */
    private void bolaLenta() {
        if (!isPasoTiempo()) {
            if (contTimer == 0) {
                bola.setSpeed(1);
                contTimer++;
            } else if (tiempo % 10000 == 0) {
                if (esInicio) {
                    bola.setSpeed(speedPuntos);
                    speedPuntos = 0;
                }
                setPasoTiempo(true);
            }
        }
    }

    /**
     * metodo que detiene la bola cuando intersecta una capsula verde
     */
    private void detenerBola() {
        bola.setSpace(0);
        bola.mover(0);
        bola.setX(raqueta.getX() + 40);
    }
    /**
     * metodo que obtiene el tiempo limite del uso del laser
     */
    private void tiempoLaser(){
        if (tiempo%10000 == 0) {
            setTocoCapsula(false);
        }
    }

    /**
     * metodo que se encargar de cargar el del cañon laser
     *
     * @param g Graphics
     */
    public void canonLaser(Graphics g) {
        if (isActivarLaser()) {
            laser1.add(new Laser(raqueta.getX() - 3, raqueta.getY() - 15, raqueta.getX() - 3, raqueta.getY() + 15, Color.ORANGE, 2));
            laser2.add(new Laser(raqueta.getX() + (raqueta.getBase() * 3), raqueta.getY() - 15, raqueta.getX() + (raqueta.getBase() * 3), raqueta.getY() + 15, Color.ORANGE, 2));
            setActivarLaser(false);
        }
        for (int i = 0; i < laser1.size(); i++) {
            if (laser1.get(i) != null) {
                laser1.get(i).paint(g);
                laser1.get(i).setMover(1);
                laser1.get(i).mover();
            }
            if (laser1.get(i).getY2() > 660) {
                setContTurnosLaser(0);
            }
        }
        for (int i = 0; i < laser2.size(); i++) {
            if (laser2.get(i) != null) {
                laser2.get(i).paint(g);
                laser2.get(i).setMover(1);
                laser2.get(i).mover();
            }
        }
    }

    /**
     * metodo que me verifica si laser de la izquierda intersecto con una figura
     */
    public void intersectoLaser1() {
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                for (int i = 0; i < laser1.size(); i++) {
                    if (laser1.get(i) != null && mapa[x][y] != null && laser1.get(i).getBounds().intersects(mapa[x][y].getBounds())) {
                        mapa[x][y].setCantColiciones(mapa[x][y].getCantColiciones() - 1);
                        if (mapa[x][y].getCantColiciones() == 0) {
                            mapa[x][y].setEsColicion(false);
                            if (!mapa[x][y].isEsColicion()) {
                                if (estaCapsula(x, y) != (-1)) {
                                    capsula[estaCapsula(x, y)].setTocoBlocke(true);
                                }
                                puntaje += mapa[x][y].getPuntos();
                                mapa[x][y] = null;
                                laser1.remove(i);
                                espacLlenos--;
                                if (espacLlenos == 0) {
                                    siguienteNivel();
                                }
                            }
                        }
                    }
                }

            }
        }
        for (int i = 0; i < laser1.size(); i++) {//si el laser toca ningun cuadro se elimina
            if (laser1.get(i) != null && laser1.get(i).getY1() < 100) {
                laser1.remove(i);
            }
        }
    }

    /**
     * metodo que me verifica si laser de la izquierda intersecto con una figura
     */
    public void intersectoLaser2() {
        for (int x = 0; x < mapa.length; x++) {
            for (int y = 0; y < mapa[x].length; y++) {
                for (int j = 0; j < laser2.size(); j++) {
                    if (laser2.get(j) != null && mapa[x][y] != null && laser2.get(j).getBounds().intersects(mapa[x][y].getBounds())) {
                        mapa[x][y].setCantColiciones(mapa[x][y].getCantColiciones() - 1);
                        if (mapa[x][y].getCantColiciones() == 0) {
                            mapa[x][y].setEsColicion(false);
                            if (!mapa[x][y].isEsColicion()) {
                                if (estaCapsula(x, y) != (-1)) {
                                    capsula[estaCapsula(x, y)].setTocoBlocke(true);
                                }
                                puntaje += mapa[x][y].getPuntos();
                                espacLlenos--;
                                mapa[x][y] = null;
                                laser2.remove(j);
                                if (espacLlenos == 0) {
                                    siguienteNivel();
                                }
                            }
                        }

                    }
                }
            }
        }
        for (int i = 0; i < laser2.size(); i++) {//si el laser toca ningun cuadro se elimina
            if (laser2.get(i) != null && laser2.get(i).getY1() < 100) {
                laser2.remove(i);
            }
        }
    }
    /**
     * metodo que resetea el linkedlist del laser
     */
    public void resetiarLaser() {
        for (int i = 0; i < laser1.size(); i++) {
            if (laser1.get(i) != null) {
                laser1.remove(i);
            }
        }
        for (int i = 0; i < laser2.size(); i++) {
            if (laser2.get(i) != null) {
                laser2.remove(i);
            }
        }
    }

    public boolean isEsInicio() {
        return esInicio;
    }

    public void setEsInicio(boolean esInicio) {
        this.esInicio = esInicio;
    }

    public boolean isPerdioTurno() {
        return perdioTurno;
    }

    public void setPerdioTurno(boolean perdioTurno) {
        this.perdioTurno = perdioTurno;
    }

    public boolean isSiguientNivel() {
        return siguientNivel;
    }

    public void setSiguientNivel(boolean siguientNivel) {
        this.siguientNivel = siguientNivel;
    }

    public boolean isEsPausa() {
        return esPausa;
    }

    public void setEsPausa(boolean esPausa) {
        this.esPausa = esPausa;
    }

    public boolean isReiniciar() {
        return reiniciar;
    }

    public void setReiniciar(boolean reiniciar) {
        this.reiniciar = reiniciar;
    }

    public Circulo getBola() {
        return bola;
    }

    public Raqueta getRaqueta() {
        return raqueta;
    }

    public boolean isIntersectoConRaqueta() {
        return intersectoConRaqueta;
    }

    public void setIntersectoConRaqueta(boolean intersectoConRaqueta) {
        this.intersectoConRaqueta = intersectoConRaqueta;
    }

    public boolean isTocoCapsulaRaqueta() {
        return tocoCapsulaRaqueta;
    }

    public void setTocoCapsulaRaqueta(boolean tocoCapsulaRaqueta) {
        this.tocoCapsulaRaqueta = tocoCapsulaRaqueta;
    }

    public boolean isPasoTiempo() {
        return pasoTiempo;
    }

    public void setPasoTiempo(boolean pasoTiempo) {
        this.pasoTiempo = pasoTiempo;
    }

    public boolean isActivarLaser() {
        return activarLaser;
    }

    public void setActivarLaser(boolean activarLaser) {
        this.activarLaser = activarLaser;
    }

    public int getContTurnosLaser() {
        return contTurnosLaser;
    }

    public void setContTurnosLaser(int contTurnosLaser) {
        this.contTurnosLaser = contTurnosLaser;
    }
    public boolean isTocoCapsula() {
        return tocoCapsula;
    }

    public void setTocoCapsula(boolean tocoCapsula) {
        this.tocoCapsula = tocoCapsula;
    }
}
