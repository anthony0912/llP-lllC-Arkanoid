/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Anthony
 */
public class Mapa {

    private final int BASE = 40;
    private final int ALTURA = 15;

    private int x;
    private int y;
    private int cantColiciones;
    private int tipoPatron;
    private char tipo;
    private char tipoCapsula;
    private boolean esColicion;
    private int puntos;

    public Mapa() {
    }

    public Mapa(int x, int y, int cantColiciones, int tipoPatron, char tipo, char tipoCapsula, boolean esColicion, int puntos) {
        this.x = x;
        this.y = y;
        this.cantColiciones = cantColiciones;
        this.tipoPatron = tipoPatron;
        this.tipo = tipo;
        this.tipoCapsula = tipoCapsula;
        this.esColicion = esColicion;
        this.puntos = puntos;
    }

    public void paint(Graphics g) {
        tipo(g, tipo);

    }

    /**
     * 
     * @param g
     * @param tipo 
     */
    private void tipo(Graphics g, char tipo) {
        switch (tipo) {
            case 'b':
                g.setColor(Color.GRAY);
                break;
            case 'y':
                g.setColor(Color.YELLOW);
                break;
            case 'w':
                g.setColor(Color.WHITE);
                break;
            case 'o':
                g.setColor(Color.ORANGE);
                break;
            case 'c':
                g.setColor(Color.CYAN);
                break;
            case 'g':
                g.setColor(Color.GREEN);
                break;
            case 'r':
                g.setColor(Color.RED);
                break;
            case 'a':
                g.setColor(Color.BLUE);
                break;
            case 'm':
                g.setColor(Color.MAGENTA);
                break;
            default:
                break;
        }
        g.fillRect(x, y, BASE, ALTURA);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, BASE, ALTURA);
        g.setColor(Color.WHITE);
        g.drawRect(x, y, BASE + 1, ALTURA + 1);

    }

    public Rectangle getBounds() {
        Rectangle temp = new Rectangle(x, y, BASE, ALTURA);
        return temp.getBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getCantColiciones() {
        return cantColiciones;
    }

    public void setCantColiciones(int cantColiciones) {
        this.cantColiciones = cantColiciones;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    public int getTipoPatron() {
        return tipoPatron;
    }

    public void setTipoPatron(int tipoPatron) {
        this.tipoPatron = tipoPatron;
    }

    public boolean isEsColicion() {
        return esColicion;
    }

    public void setEsColicion(boolean esColicion) {
        this.esColicion = esColicion;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public char getTipoCapsula() {
        return tipoCapsula;
    }

    public void setTipoCapsula(char tipoCapsula) {
        this.tipoCapsula = tipoCapsula;
    }

    @Override
    public String toString() {
        return "Mapa{" + "BASE=" + BASE + ", ALTURA=" + ALTURA + ", x=" + x + ", y=" + y + ", cantColiciones=" + cantColiciones + ", tipoPatron=" + tipoPatron + ", tipo=" + tipo + ", tipoCapsula=" + tipoCapsula + ", esColicion=" + esColicion + ", puntos=" + puntos + '}';
    }

}
