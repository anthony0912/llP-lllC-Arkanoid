/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Anthony
 */
public class Rectangulo {

    private int x;
    private int y;
    private int base;
    private int altura;
    private Color color;
    private boolean esRelleno;
    
    public Rectangulo() {
    }

    public Rectangulo(int x, int y, int base, int altura, Color color, boolean esRelleno) {
        this.x = x;
        this.y = y;
        this.base = base;
        this.altura = altura;
        this.color = color;
        this.esRelleno = esRelleno;
    }
    
    /**
     * metodo que me pinta la figura con el color
     *
     * @param g Graphics
     */
    public void paint(Graphics g) {
        g.setColor(color);
        if (esRelleno) {
            g.fillRect(x, y, base, altura);
        } else {
            g.drawRect(x, y, base, altura);
        }
    }

    /**
     * metodo tipo rectangulo que de vuelve una figura enserrada en un
     * rectangulo para que pueda colicionar con otras figuras
     *
     * @return temp.getBounds() de la figura
     */
    public Rectangle getBounds() {
        Rectangle temp = new Rectangle(x, y, base, altura);
        return temp.getBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isEsRelleno() {
        return esRelleno;
    }

    public void setEsRelleno(boolean esRelleno) {
        this.esRelleno = esRelleno;
    }

    @Override
    public String toString() {
        return "Rectangulo{" + "x=" + x + ", y=" + y + ", base=" + base + ", altura=" + altura + ", color=" + color + ", esRelleno=" + esRelleno + '}';
    }
    
}
