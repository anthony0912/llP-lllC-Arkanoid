/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Anthony
 */
public class Raqueta {
    private int x;
    private int y;
    private int base;
    private int altura;
    private Color color1;
    private Color color2;
    private int mover;
    private int speed;

    public Raqueta() {
    }

    public Raqueta(int x, int y, int base, int altura, Color color1, Color color2, int speed) {
        this.x = x;
        this.y = y;
        this.base = base;
        this.altura = altura;
        this.color1 = color1;
        this.color2 = color2;
        this.speed = speed;
    }
    
    /**
     * metodo que pinta la raqueta
     * @param g Graphics
     */
    public void paint(Graphics g){
        g.setColor(color1);
        g.fillRect(x, y, base, altura);
        g.setColor(color2);
        g.fillRect(x + base, y, base, altura);
        g.setColor(color1);
        g.fillRect(x + base * 2, y, base, altura);
    }
    /**
     * metodo que hace el movimiento de los figura, 1:Arriba, 2:Abajo,
     * 3:Izquierda, 4:Derecha y 0:Parada
     */
    public void mover() {
        switch (mover) {
            case 1:
                if (y > 0) {
                    y -= speed;
                }
                break;
            case 2:
                if (y < 700) {
                    y += speed;
                }
                break;
            case 3:
                if (x > 6) {
                    x -= speed;
                }
                break;
            case 4:
                if (x < 594 - base * 3) {
                    x += speed;
                }
                break;
            default:
                break;
        }
    }
    /**
     * metodo que me hace el intersecion de la raqueta
     * @param pos int posicion del lado de la raqueta a golpear
     * @return Rectangle de un getBounds
     */
    public Rectangle getBounds(int pos){
        switch (pos) {
            case 1:
                return new Rectangle(x, y, base, altura);
            case 2:
                return new Rectangle(x + base, y, base, altura);
            case 3:
                return new Rectangle(x + base * 2, y, base, altura);
            default:
                return null;
        }
    }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    public Color getColor1() {
        return color1;
    }

    public void setColor1(Color color1) {
        this.color1 = color1;
    }

    public Color getColor2() {
        return color2;
    }

    public void setColor2(Color color2) {
        this.color2 = color2;
    }

    public int getMover() {
        return mover;
    }

    public void setMover(int mover) {
        this.mover = mover;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Raqueta{" + "x=" + x + ", y=" + y + ", base=" + base + ", altura=" + altura + ", color1=" + color1 + ", color2=" + color2 + ", mover=" + mover + ", speed=" + speed + '}';
    }
    
}
