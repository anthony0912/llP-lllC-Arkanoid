/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Anthony
 */
public class Mensaje {
    private int tamanoLetra;
    private Color color;
    private String mensaje;
    String siguentNivel = "Presiona   espacio  para  comensar  el  siguente  nivel";
    String mensajeInico = "Bienvenido   Presiona  espacio  para  jugar";
    String score  =  "score ";
    String pausaTablero = "P   Pausa";
    String reiniciarTablero = "R   Reiniciar Juego";
    String vida = "Vida ";
    String pausa = "PAUSA";
    String nivel = "Nivel";
    String comPartida = "Presiona  espacio  para  comenzar";
    int puntaje = 0;
    
    public Mensaje() {
    }

    public Mensaje(int tamanoLetra, Color color) {
        this.tamanoLetra = tamanoLetra;
        this.color = color;
        
    }
    
    public void paint(Graphics g){
        Font font = new Font("ArcadeClassic", Font.BOLD,tamanoLetra);
        g.setFont(font);
        g.setColor(color);
        g.drawString(score, 20, 20);
        g.drawString(nivel, 150, 20);
        g.drawString(vida, 280, 20);
        g.drawString("Presiona  ", 420, 20);
        g.drawString(pausaTablero, 420, 40);
        g.fillRect(434, 35, 3, 1);
        g.drawString(reiniciarTablero, 420, 60);
        g.fillRect(434, 55, 3, 1);
    }

    public int getTamanoLetra() {
        return tamanoLetra;
    }

    public void setTamanoLetra(int tamanoLetra) {
        this.tamanoLetra = tamanoLetra;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Mensaje{" + "tamanoLetra=" + tamanoLetra + ", color=" + color + ", mensaje=" + mensaje + '}';
    }
    
}
