/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import javax.swing.JFrame;

/**
 *
 * @author Anthony
 */
public class Arkanoid {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame fr = new JFrame("Arkanoid");
        fr.setDefaultCloseOperation(fr.EXIT_ON_CLOSE);
        fr.add(new Panel());
        fr.pack();
        fr.setLocationRelativeTo(null);
        fr.setVisible(true);
        while (true) {
            fr.repaint();
            Thread.sleep(10);
        }
    }

}
