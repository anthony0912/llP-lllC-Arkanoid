/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Anthony
 */
public class Capsula {

    private final int RADIO = 10;

    private int x;
    private int y;
    private char tipo;
    private int mover;
    private int speed;
    private boolean tocoBlocke;

    public Capsula() {
        speed = 2;
    }

    public Capsula(int x, int y, char tipo, int speed, boolean tocoBlocke) {
        this.x = x;
        this.y = y;
        this.tipo = tipo;
        this.speed = speed;
        this.tocoBlocke = tocoBlocke;
    }
/**
 * 
 * @param g 
 */
    public void pintarCapsula(Graphics g) {
        Color color = null;
        switch (tipo) {
            case 'a':
                g.setColor(Color.BLUE);
                color = new Color(0, 0, 145);
                break;
            case 'r':
                g.setColor(new Color(255, 0, 0));
                color = new Color(174, 0, 0);
                break;
            case 'g':
                g.setColor(Color.GRAY);
                color = new Color(70, 70, 70);
                break;
            case 'v':
                g.setColor(new Color(0, 234, 0));
                color = new Color(0, 170, 85);
                break;
            case 'n':
                g.setColor(new Color(255, 128, 0));
                color = new Color(206, 103, 0);
                break;
            default:
                color = null;
                break;
        }

        g.fillRect(x, y, 33, 17); //barra medio
        g.fillRect(x - 4, y + 2, 4, 13); //barra izq
        g.fillRect(x + 33, y + 2, 4, 13); //barra der

        g.setColor(color);
        g.fillRect(x + 3, y + 2, 26, 12); //barra medio, peque;a
        g.fillRect(x, y + 5, 3, 7); //barra medio, peque;a izq
        g.fillRect(x + 29, y + 4, 3, 7); //barra medio, peque;a der

        g.setColor(Color.WHITE);
        g.fillRect(x, y + 2, 28, 2); //Barra larga
        g.fillRect(x - 4, y + 6, 2, 2); // Barra pequeña

        g.setColor(Color.yellow);
        g.fillRect(x + 2, y + 14, 3, 3);
    }

    /**
     * metodo que tiene el movimiento de la capsula
     */
    public void mover() {
        if (mover == 1) {
            y += speed;
        }
    }

    /**
     * 
     * @return 
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, 33, 17);
    }

    public boolean isTocoBlocke() {
        return tocoBlocke;
    }

    public void setTocoBlocke(boolean tocoBlocke) {
        this.tocoBlocke = tocoBlocke;
    }
    

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getMover() {
        return mover;
    }

    public void setMover(int mover) {
        this.mover = mover;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Capsula{" + "RADIO=" + RADIO + ", x=" + x + ", y=" + y + ", tipo=" + tipo + ", mover=" + mover + ", speed=" + speed + ", tocoBlocke=" + tocoBlocke + '}';
    }
}
