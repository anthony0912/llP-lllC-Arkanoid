/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Anthony
 */
public class Laser {

    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private Color color;
    private int speed;
    private int mover;

    public Laser() {
    }

    public Laser(int x1, int y1, int x2, int y2, Color color, int speed) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.color = color;
        this.speed = speed;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.drawLine(x1, y1, x2, y2);
        g.drawLine(x1 + 1, y1, x2 + 1, y2);
        g.drawLine(x1 + 2, y1, x2 + 2, y2);
        g.drawLine(x1 + 3, y1, x2 + 3, y2);
    }

    public void mover() {
        if (mover == 1) {
            y1 -= speed;
            y2 -= speed;
        }
    }

    public Rectangle getBounds() {
        return new Rectangle(x1, y1, 10, 10);
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getMover() {
        return mover;
    }

    public void setMover(int mover) {
        this.mover = mover;
    }

    @Override
    public String toString() {
        return "Laser{" + "x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + ", color=" + color + ", speed=" + speed + ", mover=" + mover + '}';
    }
}
