/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arkanoid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

/**
 *
 * @author Anthony
 */
public class Circulo {

    private int x;
    private int y;
    private int radio;
    private Color color;
    private boolean esRelleno;
    private int speedX;
    private int speedY;
    private int speed;
    private boolean rebotar;
    private boolean rotar;
    private int space;
    private int pausa;

    public Circulo() {
        space = 0;
        pausa = 0;
    }

    public Circulo(int x, int y, int radio, Color color, boolean esRelleno, int speedX, int speedY, int speed, boolean rebotar, boolean rotar) {
        this.x = x;
        this.y = y;
        this.radio = radio;
        this.color = color;
        this.esRelleno = esRelleno;
        this.speedX = speedX;
        this.speedY = speedY;
        this.speed = speed;
        this.rebotar = rebotar;
        this.rotar = rotar;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        if (esRelleno) {
            g.fillOval(x, y, radio * 2, radio * 2);
        } else {
            g.drawOval(x, y, radio * 2, radio * 2);
        }
    }
    public void mover(int keyChar) {
        switch (keyChar) {
            case 1:
                if (speedY <= 0) {
                    speedY = -speed;
                } else {
                    speedY *= -1;
                }
                break;
            case 2:
                if (speedY >= 0) {
                    speedY = speed;
                } else {
                    speedY *= -1;
                }
                break;
            case 3:
                if (speedX <= 0) {
                    speedX = -speed;
                } else {
                    speedX *= -1;
                }
                break;
            case 4:
                if (speedX >= 0) {
                    speedX = speed;
                } else {
                    speedX *= -1;
                }
                break;
            case KeyEvent.VK_SPACE:
                if (space == 0) {
                    if (speedY <= 0) {
                        speedY = -speed;
                    } else {
                        speedY *= -1;
                    }
                    space++;
                }
                break;
            case 0:
                speedX = 0;
                speedY = 0;
                break;
        }

    }

    public void mover(int ancho, int alto) {

        x += speedX;
        y += speedY;

        if (rebotar) {
            if (x <= 0 && speedX < 0) {
                speedX *= -1;
            }
            if (x >= ancho - (radio * 2) && speedX > 0) {
                speedX *= -1;
            }
        }
        if (rotar) {
            if (y >= alto - (radio * 2) && speedY > 0) {
                speedY *= -1;
            }

            if (y <= 98 && speedY < 0) {
                speedY *= -1;
            }
        }
    }

    /**
     * metodo tipo rectangulo que de vuelve una figura enserrada en un
     * rectangulo para que pueda colicionar con otras figuras
     *
     * @return temp.getBounds() de la figura
     */
    public Rectangle getBounds() {
        Rectangle temp = new Rectangle(x, y, radio * 2, radio * 2);
        return temp.getBounds();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRadio() {
        return radio * 2;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isEsRelleno() {
        return esRelleno;
    }

    public void setEsRelleno(boolean esRelleno) {
        this.esRelleno = esRelleno;
    }

    public int getSpeedX() {
        return speedX;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

    public boolean isRebotar() {
        return rebotar;
    }

    public void setRebotar(boolean rebotar) {
        this.rebotar = rebotar;
    }

    public boolean isRotar() {
        return rotar;
    }

    public void setRotar(boolean rotar) {
        this.rotar = rotar;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public int getPausa() {
        return pausa;
    }

    public void setPausa(int pausa) {
        this.pausa = pausa;
    }

    @Override
    public String toString() {
        return "Circulo{" + "x=" + x + ", y=" + y + ", radio=" + radio + ", color=" + color + ", esRelleno=" + esRelleno + ", speedX=" + speedX + ", speedY=" + speedY + ", speed=" + speed + ", rebotar=" + rebotar + ", rotar=" + rotar + ", space=" + space + ", pausa=" + pausa + '}';
    }
}
